extends Node2D

signal new_tile(tile)
signal reset

export(PackedScene) var mine_scene
export(PackedScene) var normal_tile_scene
export(int) var map_height
export(int) var map_length
export(int) var dificulty_percentage

onready var hbox = get_node("hbox")

onready var bot = get_node("bot")

var map_list = []
var tile_to_location_map = {}
var num_revealed = 0
var num_of_non_mines = 0
var num_of_mines = 0

var all_tile = []

func _ready():
	randomize()
	bot.main = self
	_load_input()
	_start()


func _load_input():
	map_height = global_script.height
	map_length = global_script.width
	dificulty_percentage = global_script.dificulty


func _start():
	_make_map()
	_set_up_bot()


func _set_up_bot():
	bot.num_of_mines = num_of_mines
	bot.tiles = map_list
	bot.set_up()


func _make_map():
	_populate_map_with_null()
	_add_mines()
	_add_normal_tiles()
	_add_tiles_as_children()


func _populate_map_with_null():
	for length_index in range(map_length):
		map_list.append([])
		for _height_index in range(map_height):
			map_list[length_index].append(null)


func _add_mines():
	num_of_mines = ((map_height * map_length) / 100) * dificulty_percentage
	for _i in range(num_of_mines):
		_add_mine()


func _add_mine():
	var random_height_index = randi() % map_height
	var random_length_index = randi() % map_length
	
	var tile = map_list[random_length_index][random_height_index]
	if tile != null:
		_add_mine()
	
	var mine = mine_scene.instance()
	assert(mine.connect("mine_pressed", self, "_on_mine_pressed") == OK)
	assert(mine.connect("mine_pressed", bot, "_on_mine_pressed") == OK)
	
	map_list[random_length_index][random_height_index] = mine
	tile_to_location_map[mine] = [random_length_index, random_height_index]


func _add_normal_tiles():
	for row_index in range(map_list.size()):
		for column_index in range(map_list[row_index].size()):
			if map_list[row_index][column_index] == null:
				num_of_non_mines += 1
				_add_normal_tile(row_index, column_index)


func _add_normal_tile(row_index, column_index):
	var neighbours = _get_neighbours(row_index, column_index)
	var num_of_neibour_mines = _get_num_of_surounding_mines(neighbours)
	var tile = normal_tile_scene.instance()
	tile.set_num_of_mines(num_of_neibour_mines)
	map_list[row_index][column_index] = tile
	tile_to_location_map[tile] = [row_index, column_index]
	
	assert(tile.connect("revealed", self, "_on_tile_revealed") == OK)
	
	if num_of_neibour_mines == 0:
		assert(tile.connect("zero_tile_clicked", self, "on_zero_ckicked_cascade_controler") == OK)


func _get_neighbours(row_index, column_index):
	var neighbours = []
	for y_diff in range(-1,2):
		var y_index = row_index + y_diff
		for x_diff in range(-1,2):
			var x_index = column_index + x_diff
			if y_index < map_length and y_index >= 0 and x_index < map_height and x_index >= 0:
				neighbours.append(map_list[y_index][x_index])
	
	return neighbours


func _get_num_of_surounding_mines(tile_list):
	var num_of_neibour_mines = 0
	for tile in tile_list:
		if tile and tile.is_in_group("mines"):
			num_of_neibour_mines += 1
	return num_of_neibour_mines


func _add_tiles_as_children():
	for x_list in map_list:
		var vertical_constainer = VBoxContainer.new()
		hbox.add_child(vertical_constainer)
		for tile in x_list:
			vertical_constainer.add_child(tile)
			all_tile.append(tile)
			emit_signal("new_tile", tile)


func on_zero_ckicked_cascade_controler(tile):
	var tile_coords = tile_to_location_map[tile]
	var neighbours = _get_neighbours(tile_coords[0], tile_coords[1])
	for neighbour in neighbours:
		if not neighbour.is_in_group("mines"):
			neighbour.reveal()


func _on_mine_pressed():
	get_tree().call_group("mines", "reveal_mine")


func _reset():
	_delete_tile()
	_reset_variables()
	_start()
	emit_signal("reset")


func _delete_tile():
	for tile in all_tile:
		tile.queue_free()
	
	for obj in hbox.get_children():
		hbox.remove_child(obj)
		obj.queue_free()


func _reset_variables():
	num_revealed = 0
	num_of_non_mines = 0
	map_list = []
	all_tile = []


func _on_tile_revealed():
	num_revealed += 1
	if num_revealed == num_of_non_mines:
		print("win")
		_reset()


func _on_hud_reset_from_hud():
	_reset()
