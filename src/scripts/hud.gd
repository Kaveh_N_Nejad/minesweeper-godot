extends Control

signal reset_from_hud
signal use_bot
signal stop_bot

onready var click_count_label = get_node("click_count_label")
onready var use_bot_checkbox = get_node("use_bot_checkbox")

var num_clicked = 0

func _on_main_new_tile(tile):
	assert(tile.connect("signal_clicked", self, "_on_tile_clicked") == OK)


func _on_tile_clicked():
	num_clicked += 1
	click_count_label.text = "clicked: " + str(num_clicked)


func _on_reset_button_pressed():
	emit_signal("reset_from_hud")


func _on_main_reset():
	num_clicked = 0
	click_count_label.text = "clicked: " + str(num_clicked)
	_on_use_bot_checkbox_toggled(use_bot_checkbox.is_pressed())


func _on_use_bot_checkbox_toggled(button_pressed):
	if button_pressed:
		emit_signal("use_bot")
	else:
		emit_signal("stop_bot")
