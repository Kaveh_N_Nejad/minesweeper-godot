extends Node

onready var timer = get_node("Timer")
var main

var tiles = []
var mine_likelihood_to_tiles_map = {}
var tile_to_location_map = {}
var map_height
var map_length

var num_flagged = 0
var num_of_mines = 0
var num_revealed = 0
var num_of_tiles = 0

func _choose_tile():
	_reset_vars()
	_update_vars()
	_assign_mine_likelihood_of_all()
	_flag_all_100_percent_likely()
	_reveal_lowest_number_or_random()


func set_up():
	for row in tiles:
		num_of_tiles += row.size()


func play():
	timer.start() 


func stop():
	timer.stop()


func _reveal_lowest_number_or_random(force = false):
	var keys = mine_likelihood_to_tiles_map.keys()
	
	if keys.size() == 0:
		_reveal_random()
		return
	
	keys.sort()
	
	var lowest_val_key = keys.front()
	
	var likelyhood_of_random_being_mine = get_chance_of_random_being_mine()
	
	if lowest_val_key < likelyhood_of_random_being_mine or force:
		var tile = mine_likelihood_to_tiles_map[lowest_val_key].front()
		_reveal_tile(tile)
		return
	
	_reveal_random()


func get_chance_of_random_being_mine():
	var num_of_unkown_mines = num_of_mines - num_flagged
	var num_of_unknown_tiles = num_of_tiles - num_flagged - num_revealed
	var likelyhood_of_random_being_mine = num_of_unkown_mines / float(num_of_unknown_tiles)
	return likelyhood_of_random_being_mine


func _reveal_random():
	var tiles_in_likelyhood_dict = _get_all_tiles_in_mine_likelihood_to_tiles_map()
	for row in tiles:
		for tile in row:
			if not (tile in tiles_in_likelyhood_dict) and not tile.revialed and not tile.is_flaged:
				_reveal_tile(tile)
				return
	
	_reveal_lowest_number_or_random(true)


func _get_all_tiles_in_mine_likelihood_to_tiles_map():
	var tiles_in_dict = []
	for key in mine_likelihood_to_tiles_map.keys():
		tiles_in_dict += mine_likelihood_to_tiles_map[key]
	
	return tiles_in_dict


func _reveal_tile(tile):
	tile.on_clicked()


func _flag_all_100_percent_likely():
	if not (1.0 in mine_likelihood_to_tiles_map.keys()):return
	var tiles_to_flag = mine_likelihood_to_tiles_map[1.0]

	for tile_to_flag in tiles_to_flag:
		tile_to_flag._flag()
		num_flagged += 1


func _update_vars():
	tiles = main.map_list
	tile_to_location_map = main.tile_to_location_map
	map_height = main.map_height
	map_length = main.map_length
	num_revealed = main.num_revealed


func _reset_vars():
	tiles = []
	mine_likelihood_to_tiles_map = {}
	tile_to_location_map = {}
	map_height = null
	map_length = null
	num_revealed = 0


func _assign_mine_likelihood_of_all():
	for row in tiles:
		for tile in row:
			_assign_mine_likelihood_to_tile(tile)


func _assign_mine_likelihood_to_tile(tile):
	if tile.revialed or tile.is_flaged: return
	var tile_neigbours = _get_tile_neighbours(tile)
	var largest_likelyhood = _get_tile_mine_likelihood_from_each_neighbours(tile_neigbours)
	
	if largest_likelyhood == null:return
	
	if not (largest_likelyhood in mine_likelihood_to_tiles_map.keys()):
		mine_likelihood_to_tiles_map[largest_likelyhood] = []
	
	tile.set_likelyhood_text(largest_likelyhood)
	mine_likelihood_to_tiles_map[largest_likelyhood].append(tile)


func _get_tile_mine_likelihood_from_each_neighbours(neighbours):
	var largest_likelihood = 0
	var found = false
	for neighbour in neighbours:
		var new_likelihood = _get_likelihood_from_neighbour(neighbour)
		if new_likelihood != null:
			found = true
			
			if new_likelihood == 0: return new_likelihood
			
			if new_likelihood > largest_likelihood:
				largest_likelihood = new_likelihood
	if found:
		return largest_likelihood
	return null


func _get_likelihood_from_neighbour(tile):
	if not tile.revialed: return null
	var tile_number = tile.num_of_mines
	if tile_number == 0: return null
	
	
	var tile_neigbours = _get_tile_neighbours(tile)
	var num_of_revealed_neigbours = get_num_of_revealed_from_list(tile_neigbours)
	var num_of_flaged_neigbours = _get_num_of_flagged_from_list(tile_neigbours)
	var unknown_mines = tile_number - num_of_flaged_neigbours
	
	var num_of_unknown_tiles = tile_neigbours.size() - num_of_flaged_neigbours - num_of_revealed_neigbours	
	
	
	var likelihood = (float(unknown_mines) / num_of_unknown_tiles)
	return likelihood


func get_num_of_revealed_from_list(list):
	var num = 0
	for tile in list:
		if tile.revialed:
			num += 1
	
	return num


func _get_num_of_flagged_from_list(list):
	var num = 0
	for tile in list:
		if tile.is_flaged:
			num += 1
	
	return num


func _get_tile_neighbours(tile):
	var tile_pos = tile_to_location_map[tile]
	var row_index = tile_pos[0]
	var column_index = tile_pos[1]
	var neighbours = []
	
	for y_diff in range(-1,2):
		var y_index = row_index + y_diff
		for x_diff in range(-1,2):
			var x_index = column_index + x_diff
			if y_index < map_length and y_index >= 0 and x_index < map_height and x_index >= 0:
				neighbours.append(tiles[y_index][x_index])
	neighbours.erase(tile)
	return neighbours


func _on_Timer_timeout():
	_choose_tile()


func _on_mine_pressed():
	timer.stop()
