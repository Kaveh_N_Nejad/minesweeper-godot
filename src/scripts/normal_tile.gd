extends "res://src/scripts/base_tile.gd"

onready var tile_label = get_node("tile_label")
signal revealed
signal zero_tile_clicked(tile)

var num_of_mines = null

func update_label_text():
	tile_label.text = str(num_of_mines)


func set_num_of_mines(new_num_of_mines):
	num_of_mines = new_num_of_mines


func on_clicked():
	if is_flaged: return
	if revialed: return
	.on_clicked()
	reveal()


func reveal():
	if revialed: return
	if is_flaged: return
	.reveal()
	update_label_text()
	emit_signal("revealed")
	tile_label.visible = true
	if num_of_mines == 0:
		emit_signal("zero_tile_clicked", self)
	
