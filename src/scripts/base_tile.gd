extends Control

onready var tile_button = get_node("tile_button")
onready var flag = get_node("flag")
onready var likelyhood_label = get_node("likelyhood_label")

signal signal_clicked

var clicked = false
var revialed = false
var is_flaged = false


func _on_tile_button_gui_input(event):
	if event is InputEventMouseButton and event.pressed:
		match event.button_index:
			BUTTON_LEFT:
				if Input.is_action_pressed("shift"):
					_toggle_flag()
					return
				on_clicked()
			BUTTON_RIGHT:
				_toggle_flag()


func on_clicked():
	if is_flaged: return
	if revialed: return
	clicked = true
	emit_signal("signal_clicked")


func _toggle_flag():
	if is_flaged:
		_unflag()
	else:
		_flag()


func _unflag():
	is_flaged = false
	flag.visible = false


func _flag():
	is_flaged = true
	flag.visible = true


func is_clicked():
	return clicked


func reveal():
	if revialed: return
	likelyhood_label.text = ""
	tile_button.disabled = true
	tile_button.visible = false
	revialed = true


func set_likelyhood_text(num):
	num  = round(num * pow(10.0, 2)) / pow(10.0, 2)
	likelyhood_label.text = str(num)
