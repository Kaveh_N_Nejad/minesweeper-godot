extends Control

onready var width_input = get_node("width input")
onready var height_input = get_node("height input")
onready var dificulty_input = get_node("dificulty input")

var main_scene = "res://src/scenes/main.tscn"

func _on_submit_button_pressed():
	var width = width_input.text
	var height = height_input.text
	var dificulty = dificulty_input.text
	
	var valide_input = validate_input(width, height, dificulty)
	
	width = valide_input["width"]
	height = valide_input["height"]
	dificulty = valide_input["dificulty"]
	
	global_script.height = height
	global_script.width = width
	global_script.dificulty = dificulty
	assert(get_tree().change_scene(main_scene) == OK)


func validate_input(width, height, dificulty):
	if width.is_valid_integer():
		width = int(width)
	else:
		width = 15
	
	if height.is_valid_integer():
		height = int(height)
	else:
		height = 10
	
	if dificulty.is_valid_integer():
		dificulty = int(dificulty)
	else:
		dificulty = 35
	
	if width < 2:
		width = 2
	
	if height < 2:
		height = 2
	
	if dificulty < 1:
		dificulty = 1 
	
	return {"width" :  width, "height" : height, "dificulty" : dificulty}


func _on_CheckButton_toggled(button_pressed):
	global_script.use_bot = button_pressed
