extends "res://src/scripts/base_tile.gd"

signal mine_pressed

onready var mine_sprite = get_node("mine_sprite")


func on_clicked():
	if is_flaged: return
	if revialed: return
	.on_clicked()
	reveal_mine()
	likelyhood_label.text = likelyhood_label.text + "NO"
	emit_signal("mine_pressed")


func reveal_mine():
	tile_button.visible = false
	mine_sprite.visible = true
	
